use strict;
use warnings;
use Test::More 0.96;
use Test::Deep;

use Auth::JWT::Azure;
use Sub::Override;

my $azure = Auth::JWT::Azure->new(
  application_id => 'd27499c5-7ed6-45e6-900a-5c0463b7358e',
  tenant_id      => 'b8014aaa-50bb-4da4-8b89-115662852027',
  resource_id    => '64c764f5-469f-455b-ab3d-c32af8a44d24',
);

my $count = 0;
my $override = Sub::Override->new(
  'Auth::JWT::Azure::get_kid_key' => sub {
    my $self = shift;
    my $uri = shift;
    return { keys => { $count++ => $uri } }
  }
);

my $keys = $azure->get_kid_keys();

cmp_deeply(
  $keys,
  {
    keys => [
      {
        0 =>
          'https://login.microsoftonline.com/b8014aaa-50bb-4da4-8b89-115662852027/.well-known/openid-configuration/'
      },
      {
        1 =>
          'https://login.microsoftonline.com/b8014aaa-50bb-4da4-8b89-115662852027/.well-known/openid-configuration/?appid=d27499c5-7ed6-45e6-900a-5c0463b7358e'
      },
      {
        2 =>
          'https://login.microsoftonline.com/b8014aaa-50bb-4da4-8b89-115662852027/.well-known/openid-configuration/?appid=64c764f5-469f-455b-ab3d-c32af8a44d24'
      }
    ]
  },
  "We got our kid keys from the correct locations"
);

done_testing;
