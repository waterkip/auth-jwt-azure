package Auth::JWT::Azure;
use v5.26;
use Object::Pad;

our $VERSION = '0.001';

# ABSTRACT: Get validate JWT tokens for Azure

class Auth::JWT::Azure :does(Auth::JWT::Provider);
use LWP::UserAgent;
use JSON::XS;

field $ua :param = undef;

sub BUILDARGS {
  my $self = shift;
  my %args = @_;

  my $resource_id    = delete $args{resource_id};
  my $tenant_id      = delete $args{tenant_id};
  my $application_id = $args{application_id};

  $args{wellknown} = [
    sprintf(
      'https://login.microsoftonline.com/%s/.well-known/openid-configuration/',
      $tenant_id),
    sprintf(
      'https://login.microsoftonline.com/%s/.well-known/openid-configuration/?appid=%s',
      $tenant_id, $application_id
    ),
  ];

  $args{audience} = ["api://$application_id", $application_id,];

  if ($resource_id) {
    push(
      @{ $args{wellknown} },
      sprintf(
        'https://login.microsoftonline.com/%s/.well-known/openid-configuration/?appid=%s',
        $tenant_id, $resource_id
      )
    );
    push(@{ $args{audience} }, $resource_id);
  }

  return %args;
}

method ua() {
  return $ua if $ua;
  $ua = LWP::UserAgent->new(
    timeout           => 10,
    agent             => join('/', __PACKAGE__, $VERSION),
    allowed_protocols => [qw(https)],
  );
  return $ua;
}

1;

__END__

=head1 DESCRIPTION

An implementation if L<Auth::JWT::Provider> for Azure

=head1 SYNOPSIS

  use Auth::JWT::Azure;

  my $jwt = Auth::JWT::Azure

=head1 ATTRIBUTES

=head1 METHODS
